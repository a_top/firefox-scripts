# Overview
### Firefox scripts to restore or add new functions.

# Instructions 
### Enabling script support
In-order for Firefox to support custom scripts an autoconfig script loader is needed.
Tested with the loader from xiaoxiaoflood.

Follow the instructions to install the loader from https://github.com/xiaoxiaoflood/firefox-scripts 

The <code>utils</code> folder for extension support isn't required.


### Adding a script

 Get  the .uc.js file you want from <code>scripts</code> folder or by clicking download below, to the <code>chrome</code> folder you created in the loader installation.


 # Scripts
 

  Firefox Proton removed some options from the Application Menu, use these scripts to restore them:
 #### Open File  in Application Menu
  [Download file](https://gitlab.com/a_top/firefox-scripts/-/raw/main/scripts/restoreOpenFileInAppMenu.uc.js?inline=false).

 <h4> Restore Previous Session  in Application Menu </h4>
 The  option will not be grayed out even if there isn't a session to restore, unlike the original behavior.
 To also enable the icon, follow the instructions inside the code.

  [Download file](https://gitlab.com/a_top/firefox-scripts/-/raw/main/scripts/restoreRestorePreviousSeason.uc.js?inline=false).

