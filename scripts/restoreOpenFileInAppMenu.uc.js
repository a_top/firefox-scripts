// Restore Open File in App Menu
// based on 'restartInmenu.uc.js' script by Aris
// https://github.com/Aris-t2/CustomJSforFx/
// Works with https://github.com/xiaoxiaoflood/firefox-scripts , autoconfig script loader.


var RestoreOpenFileInAppMenu = {
  init: function() {


      restoreOpenFile = document.createXULElement("toolbarbutton");
      restoreOpenFile.setAttribute("label", "Open File…");
      restoreOpenFile.setAttribute("id","appMenu-open-file-button2");
      restoreOpenFile.setAttribute("class","subviewbutton");
      restoreOpenFile.setAttribute("shortcut", "Ctrl+O");
      restoreOpenFile.setAttribute("oncommand", "BrowserOpenFileWindow();");
      var appMenuPb =  document.querySelector("#appMenu-viewCache")?.content.querySelector("#appMenu-print-button2") || document.querySelector("#appMenu-print-button2");
      appMenuPb.before(restoreOpenFile);
      
    

  }


}


RestoreOpenFileInAppMenu.init();
