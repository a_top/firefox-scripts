// Restore Restore Previous Session
// based on 'restartInmenu.uc.js' script by Aris
// https://github.com/Aris-t2/CustomJSforFx/
// Works with https://github.com/xiaoxiaoflood/firefox-scripts , autoconfig script loader.

var {Services} = Components.utils.import("resource://gre/modules/Services.jsm", {});

var {SessionStore} = Components.utils.import("resource:///modules/sessionstore/SessionStore.jsm", {}); 

var RestoreLastSessionMenuRestorer = {
  init: function() {

    try {

      restoresession_appmenu = document.createXULElement("toolbarbutton");
      restoresession_appmenu.setAttribute("label", "Restore Previous Session");
      restoresession_appmenu.setAttribute("id","appMenu-RestoreLastSession-button");
      restoresession_appmenu.setAttribute("class","subviewbutton");
      restoresession_appmenu.setAttribute("key", "Shift+P");
      restoresession_appmenu.setAttribute("oncommand", "RestoreLastSessionMenuRestorer.restoreAndDisable();");
      var appMenuSep = document.querySelector("#appMenu-viewCache")?.content.querySelector("#appMenu-new-private-window-button2").nextSibling || document.querySelector("#appMenu-new-private-window-button2").nextSibling;
      appMenuSep.before(restoresession_appmenu);
      
    } catch(e) {}



    // Add icon ,to be used together with icon restore .css  for Proton, uncomment  the  segment below to enable!
    /* Delete this

    var sss = Components.classes["@mozilla.org/content/style-sheet-service;1"].getService(Components.interfaces.nsIStyleSheetService);

    var uri = Services.io.newURI("data:text/css;charset=utf-8," + encodeURIComponent('\
      \
      #appMenu-RestoreLastSession-button { \
            list-style-image: url("chrome://browser/skin/restore-session.svg");\
      } \
      \
    '), null, null);

    sss.loadAndRegisterSheet(uri, sss.AGENT_SHEET);

    and this*/
  },

  restoreAndDisable: function() {
    if(SessionStore.canRestoreLastSession){
      SessionStore.restoreLastSession();
    }
    let mybutton =  document.getElementById("appMenu-RestoreLastSession-button")
    mybutton.setAttribute("disabled", "true");

  }

}


RestoreLastSessionMenuRestorer.init();
